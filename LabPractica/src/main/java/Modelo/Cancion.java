/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author benja
 */
public class Cancion {

    private String tituloCancion;
    private String genero;
    private String duracion;
    private String tipo;

    public Cancion(String tituloCancion, String genero, String duracion, String tipo) {
        this.tituloCancion = tituloCancion;
        this.genero = genero;
        this.duracion = duracion;
        this.tipo = tipo;
    }

    public String getTituloCancion() {
        return tituloCancion;
    }

    public void setTituloCancion(String tituloCancion) {
        this.tituloCancion = tituloCancion;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Cancion{" + "tituloCancion=" + tituloCancion + ", genero=" + genero + ", duracion=" + duracion + ", tipo=" + tipo + '}';
    }

}
