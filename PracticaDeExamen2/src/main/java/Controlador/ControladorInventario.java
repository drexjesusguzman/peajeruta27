/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.Inventario;
import Modelo.RegistroInventarioCSV;
import Vista.PanelInventario;
import Vista.GUIInventario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JOptionPane;

/**
 *
 * @author benja
 */
public class ControladorInventario implements ActionListener, MouseListener {
    
    private RegistroInventarioCSV registroInventarioCSV;
    //private PanelInventario panelInventario;
    private GUIInventario guiInventario;
    private Inventario inventario;
    

    public ControladorInventario() {
        guiInventario = new GUIInventario();
        guiInventario.setVisible(true);
        guiInventario.escuchar(this);

        guiInventario.setLocationRelativeTo(null);
        registroInventarioCSV= new RegistroInventarioCSV();
        guiInventario.setDatosTablaInventario(this.registroInventarioCSV.getDatosTabla());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Agregar":
                if (guiInventario.getTxtPlaca().equalsIgnoreCase("")) {
                    JOptionPane.showMessageDialog(null, "Debe registrar una Placa");
                } else if (guiInventario.getTxtAccesorios().equalsIgnoreCase("")) {
                    JOptionPane.showMessageDialog(null, "Debe registrar los Accesorios");
                } else {
                    GUIInventario.mensaje(registroInventarioCSV.agregar(new Inventario(guiInventario.getTxtPlaca(), guiInventario.getTxtMarca(), guiInventario.getTxtModelo(), guiInventario.getTxtTipo(), guiInventario.getTxtEstado(), guiInventario.getTxtAccesorios())));
                    guiInventario.limpiar();
                    this.guiInventario.setDatosTablaInventario(this.registroInventarioCSV.getDatosTabla());
                }
                System.out.println("Este es el evento de agregar");
                System.out.println("Placa: " + guiInventario.getTxtPlaca() + "\nMarca: " + guiInventario.getTxtMarca() + "\nModelo: " + guiInventario.getTxtModelo() + "\nTipo: " + guiInventario.getTxtTipo() + "\nEstado: " + guiInventario.getTxtEstado() + "\nAccesorios: " + guiInventario.getTxtAccesorios());

                break;
            case "Eliminar":
                guiInventario.mensaje(registroInventarioCSV.eliminar(this.inventario));
                guiInventario.limpiar();
                this.guiInventario.setDatosTablaInventario(this.registroInventarioCSV.getDatosTabla());
                System.err.println("Este es el evento de eliminar");

                break;
            case "Modificar":
                if (inventario != null) {
                    inventario.setAccesorios(guiInventario.getTxtAccesorios());
                    this.registroInventarioCSV.escribirCSV();
                    guiInventario.mensaje("Se ha modificado los accesorios del inventario.");
                    guiInventario.limpiar();
                    this.guiInventario.setDatosTablaInventario(this.registroInventarioCSV.getDatosTabla());
                }
                break;
            case "Buscar":
                System.out.println(guiInventario.getTxtPlaca());
                inventario = (Inventario) registroInventarioCSV.buscar(guiInventario.getTxtPlaca());
                if (inventario != null) {
                    guiInventario.setTxtMarca(inventario.getMarca());
                    guiInventario.setTxtModelo(inventario.getModelo());
                    guiInventario.setTxtTipo(inventario.getTipo());
                    guiInventario.setTxtEstado(inventario.getEstado());
                    guiInventario.setTxtAccesorios(inventario.getAccesorios());
                } else {
                    guiInventario.mensaje("El estudio con el ID: " + guiInventario.getTxtPlaca() + " no se encuentra registrado.");
                }
                System.err.println("Este es el evento de buscar");
                break;
            case "Salir":
                System.exit(0);
                break;
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void mousePressed(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void mouseExited(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
//    @Override
//    public void actionPerformed(ActionEvent e) {
//        switch (e.getActionCommand()) {
//            case "Agregar":
//                if (panelInventario.getTxtPlaca().equalsIgnoreCase("")) {
//                    JOptionPane.showMessageDialog(null, "Debe registrar una Placa");
//                } else if (panelInventario.getTxtAccesorios().equalsIgnoreCase("")) {
//                    JOptionPane.showMessageDialog(null, "Debe registrar los Accesorios");
//                } else {
//                    GUIInventario.mensaje(registroInventarioCSV.agregar(new Inventario(panelInventario.getTxtPlaca(), panelInventario.getTxtMarca(), panelInventario.getTxtModelo(), panelInventario.getTxtTipo(), panelInventario.getTxtEstado(), panelInventario.getTxtAccesorios())));
//                    panelInventario.limpiar();
//                    this.panelInventario.setDatosTablaInventario(this.registroInventarioCSV.getDatosTabla());
//                }
//                System.out.println("Este es el evento de agregar");
//                System.out.println("Placa: " + panelInventario.getTxtPlaca() + "\nMarca: " + panelInventario.getTxtMarca() + "\nModelo: " + panelInventario.getTxtModelo() + "\nTipo: " + panelInventario.getTxtTipo() + "\nEstado: " + panelInventario.getTxtEstado() + "\nAccesorios: " + panelInventario.getTxtAccesorios());
//        }
//        break;
//        case "Eliminar":
//            guiInventario.mensaje(registroInventarioCSV.eliminar(this.inventario));
//            panelInventario.limpiar();
//            this.panelInventario.setDatosTablaInventario(this.registroInventarioCSV.getDatosTabla());
//            System.err.println("Este es el evento de eliminar");
//        
//                break;
//            case "Modificar":
//             if (inventario != null) {
//                inventario.setAccesorios(panelInventario.getTxtAccesorios());
//                this.registroInventarioCSV.escribirCSV();
//                guiInventario.mensaje("Se ha modificado los accesorios del inventario.");
//                panelInventario.limpiar();
//                this.panelInventario.setDatosTablaInventario(this.registroInventarioCSV.getDatosTabla());
//            }
//                break;
//            case "Buscar":
//            System.out.println(panelInventario.getTxtPlaca());
//                inventario = (Inventario) registroInventarioCSV.buscar(panelInventario.getTxtPlaca());
//                if (inventario != null) {
//                    panelInventario.setMarca(inventario.getMarca());
//                    panelInventario.setModelo(inventario.getModelo());
//                    panelInventario.setTipo(inventario.getTipo());
//                    panelInventario.setEstado(inventario.getEstado());
//                    panelInventario.setAccesorios(inventario.getAccesorios());
//                } else {
//                    guiInventario.mensaje("El estudio con el ID: " + panelInventario.getTxtPlaca()+ " no se encuentra registrado.");
//                }
//                System.err.println("Este es el evento de buscar");
//                break;
//            case "Salir":
//                System.exit(0);
//                break;
//        }
