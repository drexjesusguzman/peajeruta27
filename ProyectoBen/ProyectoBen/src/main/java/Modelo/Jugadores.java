/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author benja
 */
public class Jugadores {

    private String nombre;
    private String nickName;
    public static final String[] ETIQUETAS_JUGADORES = {"Nombre", "NickName"};

    public Jugadores() {

    }

    public Jugadores(String nombre, String nickName) {
        this.nombre = nombre;
        this.nickName = nickName;
    }

    public String setDatosJugadores(int indice) {
        switch (indice) {
            case 0: 
                return this.getNombre();
            case 1:
                return this.getNickName();
        }
        return null;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @Override
    public String toString() {
        return "Jugadores{" + "nombre=" + nombre + ", nickName=" + nickName + '}';
    }

}
