/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Vista.GUIQuienSoy;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 *
 * @author benja
 */
public class ControladorGUIQuienSoy implements ActionListener {

    private GUIQuienSoy guiQuienSoy;

    public ControladorGUIQuienSoy(GUIQuienSoy guiQuienSoy) {
        this.guiQuienSoy = guiQuienSoy;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Salir":
                
            guiQuienSoy.dispose();
            break;
        }
    }

}
