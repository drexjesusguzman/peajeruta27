/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import Modelo.Jugadores;

/**
 *
 * @author benja
 */
public class RegistroJugadores {

    private ArrayList<Jugadores> listaJugadores;
    private JSONObject baseJSONJugadores;
    private File archivo;

    public RegistroJugadores() {
        this.listaJugadores = new ArrayList();
        this.archivo = new File("Jugadores.json");
        this.leerJSON();
    }

    public void escribirJSON() {
        JSONArray arregloJugadores = new JSONArray();
        this.baseJSONJugadores = new JSONObject();
        for (Jugadores jugadores : listaJugadores) {
            JSONObject objJSONPersona = new JSONObject();
            objJSONPersona.put("Nombre", jugadores.getNombre());
            objJSONPersona.put("NickName", jugadores.getNickName());
            arregloJugadores.add(objJSONPersona);
        }
        this.baseJSONJugadores.put("listaJugadores", arregloJugadores);
        try {
            FileWriter escribir = new FileWriter(archivo);
            escribir.write(this.baseJSONJugadores.toJSONString());
            escribir.flush();
            escribir.close();
        } catch (IOException ex) {
            System.out.println("Error al escribir el Archivo.");
        }
    }

    public void leerJSON() {
        JSONParser parser = new JSONParser();
        try {
            FileReader leer = new FileReader(this.archivo);
            Object obj = parser.parse(leer);
            this.baseJSONJugadores = (JSONObject) obj;
            JSONArray arregloJugadores = (JSONArray) this.baseJSONJugadores.get("listaJugadores");
            for (Object object : arregloJugadores) {
                JSONObject objJugador = (JSONObject) object;
                Jugadores jugadores = new Jugadores();
                jugadores.setNombre(objJugador.get("Nombre").toString());
                jugadores.setNickName(objJugador.get("NickName").toString());
                this.listaJugadores.add(jugadores);
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Error al leer el Archivo.");
        } catch (IOException ex) {
            System.out.println("Error al leer el Archivo.");
        } catch (ParseException ex) {
            System.out.println("Error al leer el Archivo.");
        }
    }

    public String agregar(Object obj) {
        Jugadores jugadores = (Jugadores) obj;
        if (this.buscar(jugadores.getNombre()) == null) {
            if (this.listaJugadores.add(jugadores)) {
                this.escribirJSON();
                return "Persona agregada con éxito.";
            } else {
                return "Error al agregar.";
            }
        }
        return "La persona ya se encuentra registrada.";

    }

    public String eliminar(Object jugadores) {
        if (this.listaJugadores.remove((Jugadores) jugadores)) {
            this.escribirJSON();
            return "Jugador eliminado con éxito";
        }
        return "Error al eliminar el jugador";
    }

    public Object buscar(String nombre) {
        for (int i = 0; i < this.listaJugadores.size(); i++) {
            if (this.listaJugadores.get(i).getNombre().equalsIgnoreCase(nombre)) {
                return (Object) this.listaJugadores.get(i);
            }
        }
        return null;
    }

    @Override
    public String toString() {
        String salida = "Lista de Jugadores: \n";
        for (Jugadores jugadores : this.listaJugadores) {
            salida += jugadores + "\n";
        }
        return salida;
    }

    public String[][] getDatosTabla() {
        String[][] matrizTabla = new String[this.listaJugadores.size()][Jugadores.ETIQUETAS_JUGADORES.length];
        for (int filas = 0; filas < this.listaJugadores.size(); filas++) {
            for (int columnas = 0; columnas < matrizTabla[0].length; columnas++) {
                matrizTabla[filas][columnas] = this.listaJugadores.get(filas).setDatosJugadores(columnas);
            }
        }
        return matrizTabla;
    }

}
