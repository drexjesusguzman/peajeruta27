/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.util.logging.Level;
import java.util.logging.Logger;
import Modelo.AreaJuego;
import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;

/**
 *
 * @author benja
 */
public class ControladorJuego extends Thread {

    private AreaJuego areaJuego;
    private ControladorGUIJuego controladorGUIJuego;

    public ControladorJuego() {
    }

    public ControladorJuego(AreaJuego areaJuego, ControladorGUIJuego controladorGUIJuego) {
        this.areaJuego = areaJuego;
        this.controladorGUIJuego = controladorGUIJuego;
    }

    @Override
    public void run() {
        while (areaJuego.getPersonajePrincipal().getVidas() > 0 && !areaJuego.isGanador()) {
            try {
                for (int posicion = 0; posicion < areaJuego.getCantidadObstaculos(); posicion++) {
                    areaJuego.getObstaculo(posicion).mover();
                    if (areaJuego.isColision()) {
                        System.out.println("Vidas " + areaJuego.getPersonajePrincipal().getVidas());
                        if (areaJuego.getPersonajePrincipal().getVidas() == 0) {
                            System.out.println("Fin del juego");
                        }
                    }
                    controladorGUIJuego.actualizar();
                }

                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(ControladorJuego.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (areaJuego.isGanador()) {
                            try {
                String audioFilePath = "./src/main/resources/img/sonidoGano.wav";

                AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(audioFilePath));

                AudioFormat audioFormat = audioInputStream.getFormat();

                DataLine.Info info = new DataLine.Info(Clip.class, audioFormat);

                Clip audioClip = (Clip) AudioSystem.getLine(info);

                audioClip.open(audioInputStream);

                FloatControl volumeControl = (FloatControl) audioClip.getControl(FloatControl.Type.MASTER_GAIN);

                float volumeLevel = -20.0f;
                volumeControl.setValue(volumeLevel);

                audioClip.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
            controladorGUIJuego.comunicarGanador();
            areaJuego.getPersonajePrincipal().setY(730);
            areaJuego.getPersonajePrincipal().setX(360);
            

        } else {
            try {
                String audioFilePath = "./src/main/resources/img/sonidoSalir.wav";

                AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(audioFilePath));

                AudioFormat audioFormat = audioInputStream.getFormat();

                DataLine.Info info = new DataLine.Info(Clip.class, audioFormat);

                Clip audioClip = (Clip) AudioSystem.getLine(info);

                audioClip.open(audioInputStream);

                FloatControl volumeControl = (FloatControl) audioClip.getControl(FloatControl.Type.MASTER_GAIN);

                float volumeLevel = -20.0f;
                volumeControl.setValue(volumeLevel);

                audioClip.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
            controladorGUIJuego.comunicarPerdedor();
            areaJuego.getPersonajePrincipal().setY(730);
            areaJuego.getPersonajePrincipal().setX(360);

        }
        this.interrupt();
    }
}
