/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.RegistroJugadores;
import Vista.GUIRegistro;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import Modelo.Jugadores;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author benja
 */
public class ControladorGUIRegistro implements ActionListener, MouseListener {

    private GUIRegistro guiRegistro;
    private RegistroJugadores registroJugadores;
    private Jugadores jugadores;
    private JSONObject objetoBase;

    public ControladorGUIRegistro(GUIRegistro guiRegistro) {
        objetoBase=new JSONObject();
        this.guiRegistro = guiRegistro;
        this.jugadores= jugadores;

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Guardar":
//                this.guiRegistro.setDatosTablaJugadores(this.registroJugadores.getDatosTabla());
//                registroJugadores.escribirJSON();

                JOptionPane.showMessageDialog(null, "Se ha presionado Guardar");
                System.out.println("Se ha presionado Guardar");
                break;
            case "Registro":
//                GUIRegistro guiRegistro= new GUIRegistro();
//                guiRegistro.setDatosTablaJugadores(this.registroJugadores.getDatosTabla(),Jugadores.ETIQUETAS_JUGADORES, "Registro");

                String[][] datos=new String[1][2];
                datos[0][0]=guiRegistro.getNombre();
                datos[0][1]=guiRegistro.getNick();
                guiRegistro.setDatosTablaJugadores(datos);
                JSONObject jugador=new JSONObject();
                JSONArray registro = new JSONArray();
                
                jugador.put("Nombre", guiRegistro.getNombre());
                jugador.put("Nickname", guiRegistro.getNick());
                
                registro.add(jugador);
                
                objetoBase.put("Registro de Jugadores", registro);
                
            try {
                FileWriter escribir = new FileWriter(new File("jugadores"));
                escribir.write(this.objetoBase.toJSONString());
                escribir.flush();
                escribir.close();
                
            } catch (IOException ex) {
                System.err.println("Error al crear el arcivo");
            }
                
//                registroJugadores.leerJSON();
                JOptionPane.showMessageDialog(null, "Se ha presionado Registro");
                System.out.println("Se ha presionado Registro");
                break;
            case "Salir":
                guiRegistro.dispose();
                break;
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

}
