/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import javax.swing.ImageIcon;

/**
 *
 * @author benja
 */
public class Flecha extends Elemento {

    private int direccion;
    private boolean colisiona;

    public Flecha(ImageIcon imagen, int x, int y, int direccion) {
        super(imagen, x, y);
        this.direccion = direccion;
        this.colisiona = true;
    }

    public Flecha(int direccion, int x, int y) {
        super(x, y);
        setImagen(new ImageIcon("./src/main/resources/img/ObstaculoLeft.png"));
        this.direccion = direccion;
    }

    public int getDireccion() {
        return direccion;
    }

    public void setDireccion(int direccion) {
        this.direccion = direccion;
    }

    public boolean isColisiona() {
        return colisiona;
    }

    public void setColisiona(boolean colisiona) {
        this.colisiona = colisiona;
    }

    public void mover() {
        switch (direccion) {
            case 0:
                if (getX() < 800) {
                    setX(getX() + 15);
                } else {
                    setX(0);
                }
                break;
            case 1:
                if (getX() > 0) {
                    setX(getX() - 15);
                } else {
                    setX(800);
                }
                break;
            case 2:
                setY(getY() + 15);
                break;
            case 3:
                setY(getY() - 15);
                break;
        }

    }
}
