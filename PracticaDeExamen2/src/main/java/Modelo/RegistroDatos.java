/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author benja
 */
public interface RegistroDatos {

    public String agregar(Object objeto);

    public Object buscar(String id);
    
    public String eliminar (Object estudio);
    
    //public String modificar (String id);
    
    @Override
    public String toString();
}
