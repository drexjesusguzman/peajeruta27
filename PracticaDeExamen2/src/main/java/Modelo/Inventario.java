/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author benja
 */
public class Inventario {

    public String placa;
    public String marca;
    public String modelo;
    public String tipo;
    public String estado;
    public String accesorios;

    public static final String[] ETIQUETAS_INVENTARIO = {"Placa", "Marca", ",Modelo", "Tipo", "Accesorios"};

    public Inventario(String placa, String marca, String modelo, String tipo, String estado, String accesorios) {
        this.placa = placa;
        this.marca = marca;
        this.modelo = modelo;
        this.tipo = tipo;
        this.estado = estado;
        this.accesorios = accesorios;
    }

    public Inventario() {
    }

    public String setDatosInventario(int index) {
        switch (index) {
            case 0:
                return this.getPlaca();
            case 1:
                return this.getMarca();
            case 2:
                return this.getModelo();
            case 3:
                return this.getTipo();
            case 4:
                return this.getEstado();
            case 5:
                return this.getAccesorios();
        }
        return null;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getAccesorios() {
        return accesorios;
    }

    public void setAccesorios(String accesorios) {
        this.accesorios = accesorios;
    }

    @Override
    public String toString() {
        return "Inventario{" + "placa=" + placa + ", marca=" + marca + ", modelo=" + modelo + ", tipo=" + tipo + ", estado=" + estado + ", accesorios=" + accesorios + '}';
    }

}
