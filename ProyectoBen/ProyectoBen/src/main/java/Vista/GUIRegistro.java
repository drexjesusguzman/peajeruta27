/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package Vista;

import Controlador.ControladorGUIRegistro;
import java.awt.Color;
import java.awt.event.MouseListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author benja
 */
public class GUIRegistro extends javax.swing.JFrame {

    /**
     * Creates new form GUIRegistro
     */
    private ControladorGUIRegistro controladorGUIRegistro;

    public GUIRegistro() {
        initComponents();
        controladorGUIRegistro = new ControladorGUIRegistro(this);
        escuchar(controladorGUIRegistro);
        setLocationRelativeTo(this);
        txtName.setBackground(new Color(0, 0, 0, 0));
        txtNickName.setBackground(new Color(0, 0, 0, 0));
    }

    private void escuchar(ControladorGUIRegistro controladorGUIRegistro) {
        btnSalir.addActionListener(controladorGUIRegistro);
        btnGuardar.addActionListener(controladorGUIRegistro);
        btnRegistro.addActionListener(controladorGUIRegistro);
        txtName.addActionListener(controladorGUIRegistro);
        txtNickName.addActionListener(controladorGUIRegistro);
    }
    
    public String getNombre(){
    
        return txtName.getText();
    
    }
    
    public String getNick(){
    
        return txtNickName.getText();
    
    }
    
    public void setDatosTablaJugadores(String[][] datos){
        this.tblReporte.setModel(new DefaultTableModel(datos, new String[]{"Nombre","NickName"}));
        DefaultTableModel modeloCargado=(DefaultTableModel) tblReporte.getModel();
        modeloCargado.addRow(datos);
        jScrollPane1.setViewportView(this.tblReporte);
    }
    
    public int getTableCount(){
    
        return tblReporte.getColumnCount();
    
    }
    
    public void escucharTabla(MouseListener control){
        this.tblReporte.addMouseListener(control);
}
    public String getFilaTabla(){
        int fila= this.tblReporte.getSelectedRow();
        return this.tblReporte.getModel().getValueAt(fila,0).toString();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnSalir = new javax.swing.JButton();
        txtName = new javax.swing.JTextField();
        txtNickName = new javax.swing.JTextField();
        btnGuardar = new javax.swing.JButton();
        txtTitulo = new javax.swing.JLabel();
        btnRegistro = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblReporte = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnSalir.setText(" ");
        btnSalir.setActionCommand("Salir");
        btnSalir.setContentAreaFilled(false);
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        getContentPane().add(btnSalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 550, 270, 50));

        txtName.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        txtName.setForeground(new java.awt.Color(255, 255, 255));
        txtName.setBorder(null);
        getContentPane().add(txtName, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 160, 440, 50));

        txtNickName.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        txtNickName.setForeground(new java.awt.Color(255, 255, 255));
        txtNickName.setActionCommand("<Not Set>");
        txtNickName.setBorder(null);
        txtNickName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNickNameActionPerformed(evt);
            }
        });
        getContentPane().add(txtNickName, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 250, 440, 50));

        btnGuardar.setActionCommand("Guardar");
        btnGuardar.setContentAreaFilled(false);
        getContentPane().add(btnGuardar, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 450, 290, 60));
        getContentPane().add(txtTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 30, 410, 80));

        btnRegistro.setActionCommand("Registro");
        btnRegistro.setContentAreaFilled(false);
        btnRegistro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistroActionPerformed(evt);
            }
        });
        getContentPane().add(btnRegistro, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 350, 290, 60));

        tblReporte.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Nombre", "NickName"
            }
        ));
        jScrollPane1.setViewportView(tblReporte);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 340, -1, 260));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/GUIRegistro.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 840, 630));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtNickNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNickNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNickNameActionPerformed

    private void btnRegistroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnRegistroActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSalirActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnRegistro;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblReporte;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtNickName;
    private javax.swing.JLabel txtTitulo;
    // End of variables declaration//GEN-END:variables

}
