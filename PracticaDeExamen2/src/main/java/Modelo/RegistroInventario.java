/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author benja
 */
public class RegistroInventario implements RegistroDatos{
    private ArrayList<Inventario> listaInventario;

    public RegistroInventario() {
        listaInventario = new ArrayList();
    }

    @Override
    public String agregar(Object objeto) {
        if (objeto != null) {
            Inventario inventario = (Inventario) objeto;

            if (buscar(inventario.getPlaca()) == null) {
                if (listaInventario.add(inventario)) {
                    return "El estudio ha sido registrado";
                }
            } else {
                return "El estudio ya se encuentra registrado";
            }
        }
        return "Error al ingresar el estudio";
    }

    @Override
    public Object buscar(String placa) {
        for (int i = 0; i < listaInventario.size(); i++) {
            if (listaInventario.get(i).getPlaca().equalsIgnoreCase(placa)) {
                return (Object) this.listaInventario.get(i);
            }
        }
        return null;
    }

    @Override
    public String eliminar(Object inventario) {
        if (listaInventario.remove((Inventario) inventario)) {
            return "El estudio ha sido eliminado.";
        } else {
            return "Error al eliminar el estudio.";
        }
    }

    @Override
    public String toString() {
        String salida = "Lista de estudios \n";
        for(Inventario inventario : listaInventario){
            salida += inventario + "\n";
        }
        return salida;
    }

    public String [][] getDatosTabla(){
        String [][] matrizTabla = new String [this.listaInventario.size()][Inventario.ETIQUETAS_INVENTARIO.length];
        for(int f = 0; f < this.listaInventario.size(); f++){
            for(int c = 0; c < matrizTabla[0].length; c++){
                matrizTabla[f][c] = this.listaInventario.get(f).setDatosInventario(c);
            }
        }
        return matrizTabla;
    }
}
    

