/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Vista.GUIInstrucciones;
import Vista.GUIJuego;
import Vista.GUIPrincipal;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author benja
 */
public class ControladorGUIInstrucciones implements ActionListener {

    private GUIInstrucciones guiInstrucciones;

    public ControladorGUIInstrucciones(GUIInstrucciones guiInstrucciones) {
        this.guiInstrucciones = guiInstrucciones;

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Salir":
                guiInstrucciones.dispose();
                break;
        }
    }

}
