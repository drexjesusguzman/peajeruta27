/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.Cancion;
import Vista.GUI_Zumba;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 *
 * @author benja
 */
public class ControladorGUIPrincipal implements ActionListener{
    
    private GUI_Zumba gui_zumba;
    private ArrayList <Cancion> registroCanciones;

    public ControladorGUIPrincipal() {
        
        gui_zumba=new GUI_Zumba();
        gui_zumba.setVisible(true);
        gui_zumba.setLocationRelativeTo(null);
        gui_zumba.escuchar(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        switch (e.getActionCommand()) {
            case "Guardar":
                
                System.out.println("Escucha");
                
                break;
                
            case "Cant. canciones por genero":
                
                System.out.println("Escucha");
                
                break;
                
            case "Cant. canciones por tipo":
                
                System.out.println("Escucha");
                
                break;
                
            case "Agregar cancion a lista":
                
                System.out.println("Escucha");
                
                break;
                
            case "Vaciar Lista":
                
                System.out.println("Escucha");
                
                break;
        }
    }
    
    
    
}
