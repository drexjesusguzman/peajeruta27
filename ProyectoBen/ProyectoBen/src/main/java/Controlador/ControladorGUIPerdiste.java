/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Vista.GUIJuego;
import Vista.GUIPerdiste;
import Vista.GUIPrincipal;

/**
 *
 * @author benja
 */
public class ControladorGUIPerdiste implements ActionListener {

    private GUIPerdiste guiPerdiste;
    private GUIJuego guiJuego;
    private GUIPrincipal guiPrincipal;

    public ControladorGUIPerdiste(GUIPerdiste guiPerdiste, GUIJuego guiJuego, GUIPrincipal guiPrincipal) {
        this.guiPerdiste = guiPerdiste;
        this.guiJuego = guiJuego;
        this.guiPrincipal = guiPrincipal;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Play":
                guiJuego.setVisible(true);
                break;
            case "Salir":
                System.exit(0);
                break;
        }
    }

}
