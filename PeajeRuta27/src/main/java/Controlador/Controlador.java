/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.Carro;
import Modelo.Nodo;
import Modelo.VentanillaPeaje;
import Vista.FramePeaje;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author Drexler Guzman
 */
public class Controlador implements ActionListener{

    private FramePeaje framePeaje;
    private VentanillaPeaje ventanilla;
    private int total=0, numCarros=0;
    
    public Controlador() {
        
        framePeaje=new FramePeaje();
        ventanilla=new VentanillaPeaje();
        framePeaje.escuchar(this);
        framePeaje.setVisible(true);
        framePeaje.setLocationRelativeTo(null);
        
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Agregar vehiculo":
                
                String placa=framePeaje.getTxtPlaca();
                String marca=framePeaje.getTxtMarca();
                String modelo=framePeaje.getTxtModelo();
                String tipo=framePeaje.getTxtTipo();
                
                Carro carro=new Carro(placa, marca, modelo, tipo);
                
                switch (carro.getTipo()) {
                    case "moto":
                        
                        carro.setMonto("500");
                        
                        break;

                    case "liviano":
                        
                        carro.setMonto("700");

                        break;

                    case "pesado":
                        
                        carro.setMonto("1200");

                        break;
                }
                
                    ventanilla.encolar(carro);
                    framePeaje.mostrarColaEspera(ventanilla.toString());
                    framePeaje.limpiar();
                
                break;
                
            case "Atender vehiculo":
                if(!ventanilla.isVacio()){
                
                    numCarros++;
                    
                    if(ventanilla.getCarro(0).getDato().getTipo().equals("liviano")){
                    
                        total+=700;
                        
                    }else{
                    
                        if(ventanilla.getCarro(0).getDato().getTipo().equals("moto")){
                        
                            total+=500;
                        
                        }else{
                        
                            total+=1200;
                        
                        }
                    
                    }

                    
                    ventanilla.atender();
                    framePeaje.mostrarColaEspera(ventanilla.toString());
                    framePeaje.limpiar();
                
                }else{
                
                    JOptionPane.showMessageDialog(null, "Atualmente no hay vehiculos en espera.");
                
                }
                
                break;
                
            case "Total recaudado":
                
                JOptionPane.showMessageDialog(null, "total recaudado: "+total);
                
                break;
                
            case "Total vehiculos atendidos":
                
                JOptionPane.showMessageDialog(null,"numero de vehiculos atendidos: "+numCarros);
                
                break;
                
            case "Informacion de los vehiculos":
                
                JOptionPane.showMessageDialog(null, ventanilla.getLista());
                
                break;
                
            case "Buscar":
                
                Nodo carroBuscado = null;
                for (int i = 0; i < ventanilla.getLongitud(); i++) {

                    if (ventanilla.getCarro(i).getDato().getPlaca().equals(framePeaje.getTxtPlacaConsulta())) {

                        carroBuscado = ventanilla.getCarro(i);
                    }
                }

                if (carroBuscado != null) {

                    JOptionPane.showMessageDialog(null, carroBuscado.getDato().toString());

                } else {

                    JOptionPane.showMessageDialog(null, "Vehiculo no encontrado.");

                }
                
                break;
        }
    }
    
    
    
    
}
