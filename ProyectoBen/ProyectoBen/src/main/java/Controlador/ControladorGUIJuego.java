/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import Modelo.AreaJuego;
import Modelo.Principal;
import Vista.GUIGanaste;
import Vista.GUIInstrucciones;
import Vista.GUIJuego;
import Vista.GUIPerdiste;
import Vista.PanelJuego;

/**
 *
 * @author benja
 */
public class ControladorGUIJuego implements KeyListener, MouseListener {

    private GUIJuego guiJuego;
    private PanelJuego panelJuego;
    private AreaJuego areaJuego;
    private Principal personajePrincipal;
    private ControladorJuego controladorJuego;
    private GUIPerdiste guiPerdiste;
    private GUIGanaste guiGanaste;
    private GUIInstrucciones guiInstrucciones;

    public ControladorGUIJuego() {
    }

    public ControladorGUIJuego(PanelJuego panelJuego) {

        areaJuego = new AreaJuego();
        personajePrincipal = areaJuego.getPersonajePrincipal();
        this.panelJuego = panelJuego;
        panelJuego.setControlador(this);
        controladorJuego = new ControladorJuego(areaJuego, this);
        controladorJuego.start();

    }

    public void setGUIJuego(GUIJuego guiJuego) {
        this.guiJuego = guiJuego;
    }

    public void setGUIPerdiste(GUIPerdiste guiPerdiste) {
        this.guiPerdiste = guiPerdiste;
    }

    public void setGUIGanaste(GUIGanaste guiGanaste) {
        this.guiGanaste = guiGanaste;
    }

    public void setGUIInstrucciones(GUIInstrucciones guiInstrucciones) {
        this.guiInstrucciones = guiInstrucciones;
    }

    public void dibujar(Graphics g) {
        panelJuego.setControlador(this);
        areaJuego.getFondo().show(g);
        personajePrincipal.show(g);
        for (int posicion = 0; posicion < areaJuego.getCantidadObstaculos(); posicion++) {
            areaJuego.getObstaculo(posicion).show(g);
        }

    }

    public void actualizar() {
        panelJuego.repaint();
    }

    public void comunicarPerdedor() {
        areaJuego.detener();
        guiJuego.dispose();
        guiPerdiste.setVisible(true);

    }

    public void comunicarGanador() {
        areaJuego.detener();
        guiJuego.dispose();
        System.out.println("Felicidades ha llegado a la meta");
        guiGanaste.setVisible(true);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

        if (e.getKeyCode() == KeyEvent.VK_W) {
            subir();
        } else {
            if (e.getKeyCode() == KeyEvent.VK_S) {
                bajar();
            } else {
                if (e.getKeyCode() == KeyEvent.VK_A) {
                    moverIzquierda();
                } else {
                    if (e.getKeyCode() == KeyEvent.VK_D) {
                        moverDerecha();
                    }
                }
            }

        }

        if (e.getKeyCode() == KeyEvent.VK_UP) {
            subir();
        } else {
            if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                bajar();
            } else {
                if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                    moverIzquierda();
                } else {
                    if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                        moverDerecha();
                    }
                }
            }

        }

        panelJuego.repaint();
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    public void subir() {
        personajePrincipal.setY(personajePrincipal.getY() - 20);
    }

    public void bajar() {
        personajePrincipal.setY(personajePrincipal.getY() + 20);
    }

    public void moverIzquierda() {
        personajePrincipal.setX(personajePrincipal.getX() - 20);
    }

    public void moverDerecha() {
        personajePrincipal.setX(personajePrincipal.getX() + 20);
    }

    @Override
    public void mouseClicked(MouseEvent evento) {
        System.out.println("X " + evento.getX() + "Y " + evento.getY());
    }

    @Override
    public void mousePressed(MouseEvent arg0) {
    }

    @Override
    public void mouseReleased(MouseEvent arg0) {
    }

    @Override
    public void mouseEntered(MouseEvent arg0) {
    }

    @Override
    public void mouseExited(MouseEvent arg0) {
    }
}
