/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;
import javax.swing.ImageIcon;

/**
 *
 * @author benja
 */
public class AreaJuego {

    private ArrayList<Flecha> flechas;
    private Principal personajePrincipal;
    private Elemento fondo;

    public AreaJuego() {
        flechas = new ArrayList<Flecha>();
        setElementos();

    }

    public Principal getPersonajePrincipal() {
        return personajePrincipal;
    }

    public void setElementos() {
        crearPersonajePrincipal();
        crearObstaculos();
        crearFondo();
    }

    public void crearPersonajePrincipal() {
        ImageIcon iPersonaje = new ImageIcon("./src/main/resources/img/GUIPersonaje.png");
        personajePrincipal = new Principal(iPersonaje, 360, 730, 2);
    }

    public void crearObstaculos() {
        ImageIcon iObstaculo = new ImageIcon("./src/main/resources/img/ObstaculoRight.png");
        Flecha obstaculo = new Flecha(iObstaculo, 10, 590, 0);
        flechas.add(obstaculo);
        iObstaculo = new ImageIcon("./src/main/resources/img/ObstaculoLeft.png");
        obstaculo = new Flecha(iObstaculo, 400, 460, 1);
        flechas.add(obstaculo);
        iObstaculo = new ImageIcon("./src/main/resources/img/ObstaculoRight.png");
        obstaculo = new Flecha(iObstaculo, 80, 220, 0);
        flechas.add(obstaculo);
        iObstaculo = new ImageIcon("./src/main/resources/img/ObstaculoRight.png");
        obstaculo = new Flecha(iObstaculo, 400, 590, 0);
        flechas.add(obstaculo);
        iObstaculo = new ImageIcon("./src/main/resources/img/ObstaculoLeft.png");
        obstaculo = new Flecha(iObstaculo, 80, 460, 1);
        flechas.add(obstaculo);
        iObstaculo = new ImageIcon("./src/main/resources/img/ObstaculoRight.png");
        obstaculo = new Flecha(iObstaculo, 400, 220, 0);
        flechas.add(obstaculo);

    }

    public void crearFondo() {
        ImageIcon iFondo = new ImageIcon("./src/main/resources/img/GUIFondo.png");
        fondo = new Elemento(iFondo, 0, 0);
    }
//    public void moverFondo(){
//           fondo.setX(fondo.getX()-5);
//           
//    }

    public Elemento getFondo() {
        return fondo;
    }

    public Flecha getObstaculo(int posicion) {
        return flechas.get(posicion);
    }

    public int getCantidadObstaculos() {
        return flechas.size();
    }

    synchronized public boolean isColision() {
        for (int posicion = 0; posicion < flechas.size(); posicion++) {
            if (flechas.get(posicion).isColisiona() && (personajePrincipal.getX() >= flechas.get(posicion).getX() && personajePrincipal.getX() <= flechas.get(posicion).getX() + 70 && personajePrincipal.getY() >= flechas.get(posicion).getY() && personajePrincipal.getY() <= flechas.get(posicion).getY() + 70)) {
                System.out.println("Posicion x del personaje " + personajePrincipal.getX() + "Posicion x del obstaculo " + flechas.get(posicion).getX() + "Posicion y del personaje " + personajePrincipal.getY() + "Posicion y del obstaculo " + flechas.get(posicion).getY());
                personajePrincipal.setVidas(personajePrincipal.getVidas() - 1);
                flechas.get(posicion).setColisiona(false);
                return true;
            }
        }
        return false;
    }

    public boolean isGanador() {
        if (personajePrincipal.getVidas() > 0 && personajePrincipal.getY() <= 90) {
            return true;
        }
        return false;
    }

    public void detener() {
        for (int posicion = 0; posicion < flechas.size(); posicion++) {
            flechas.get(posicion).setColisiona(false);
        }
    }
}
