/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Vista.GUIGanaste;
import Vista.GUIJuego;
import Vista.GUIPrincipal;

/**
 *
 * @author benja
 */
public class ControladorGUIGanaste implements ActionListener {

    private GUIGanaste guiGanaste;
    private GUIJuego guiJuego;
    private GUIPrincipal guiPrincipal;

    public ControladorGUIGanaste(GUIGanaste guiGanaste, GUIJuego guiJuego, GUIPrincipal guiPrincipal) {
        this.guiGanaste = guiGanaste;
        this.guiJuego = guiJuego;
        this.guiPrincipal = guiPrincipal;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Play":
                guiJuego.setVisible(true);
                break;
            case "Salir":
                System.exit(0);
                break;
        }
    }

}
