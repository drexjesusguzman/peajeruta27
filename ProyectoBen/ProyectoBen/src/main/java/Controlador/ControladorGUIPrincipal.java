/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Vista.GUIGanaste;
import Vista.GUIInstrucciones;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Vista.GUIJuego;
import Vista.GUIPerdiste;
import Vista.GUIPrincipal;
import Vista.GUIQuienSoy;
import Vista.GUIRegistro;
import Vista.PanelJuego;
import java.io.File;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;

/**
 *
 * @author benja
 */
public class ControladorGUIPrincipal implements ActionListener {

    private GUIPrincipal guiPrincipal;
    private ControladorGUIJuego controladorGUIJuego;
    private ControladorJuego controlJuego;
    private GUIJuego guiJuego;
    private GUIPerdiste guiPerdiste;
    private GUIGanaste guiGanaste;
    private PanelJuego panelJuego;
    private GUIInstrucciones guiInstrucciones;
    private GUIQuienSoy guiQuienSoy;
    private GUIRegistro guiRegistro;

    public ControladorGUIPrincipal(GUIPrincipal guiPrincipal) {
//    public ControladorGUIPrincipal(GUIPrincipal guiPrincipal, GUIInstrucciones guiInstrucciones) {
        this.guiPrincipal = guiPrincipal;
        guiJuego = new GUIJuego();
        controlJuego = new ControladorJuego();
        guiPerdiste = new GUIPerdiste(guiJuego, guiPrincipal);
        guiGanaste = new GUIGanaste(guiJuego, guiPrincipal);
        guiInstrucciones = new GUIInstrucciones(guiJuego, guiPrincipal);
        guiQuienSoy = new GUIQuienSoy(guiJuego, guiPrincipal);
        guiRegistro = new GUIRegistro();
        panelJuego = guiJuego.getPanelJuego();
        controladorGUIJuego = panelJuego.getControladorGUIJuego();
        guiJuego.setControlador(controladorGUIJuego);
        controladorGUIJuego.setGUIJuego(guiJuego);
        controladorGUIJuego.setGUIPerdiste(guiPerdiste);
        controladorGUIJuego.setGUIGanaste(guiGanaste);

    }

    public void actualizar(Graphics g) {
        controladorGUIJuego.dibujar(g);
    }

    @Override
    public void actionPerformed(ActionEvent evento) {
        switch (evento.getActionCommand()) {
            case "How to Play":
                        try {
                String audioFilePath = "./src/main/resources/img/sonidoBoton.wav";

                AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(audioFilePath));

                AudioFormat audioFormat = audioInputStream.getFormat();

                DataLine.Info info = new DataLine.Info(Clip.class, audioFormat);

                Clip audioClip = (Clip) AudioSystem.getLine(info);

                audioClip.open(audioInputStream);

                FloatControl volumeControl = (FloatControl) audioClip.getControl(FloatControl.Type.MASTER_GAIN);

                float volumeLevel = -20.0f;
                volumeControl.setValue(volumeLevel);

                audioClip.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
            guiInstrucciones.setVisible(true);

            break;
            case "Play":
                        try {
                String audioFilePath = "./src/main/resources/img/sonidoBoton.wav";
                AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(audioFilePath));
                AudioFormat audioFormat = audioInputStream.getFormat();
                DataLine.Info info = new DataLine.Info(Clip.class, audioFormat);
                Clip audioClip = (Clip) AudioSystem.getLine(info);
                audioClip.open(audioInputStream);
                FloatControl volumeControl = (FloatControl) audioClip.getControl(FloatControl.Type.MASTER_GAIN);
                float volumeLevel = -20.0f;
                volumeControl.setValue(volumeLevel);
                audioClip.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
            panelJuego.repaint();
            guiJuego.setVisible(true);
            break;
            case "Who i Am":
                        try {
                String audioFilePath = "./src/main/resources/img/sonidoBoton.wav";
                AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(audioFilePath));
                AudioFormat audioFormat = audioInputStream.getFormat();
                DataLine.Info info = new DataLine.Info(Clip.class, audioFormat);
                Clip audioClip = (Clip) AudioSystem.getLine(info);
                audioClip.open(audioInputStream);
                FloatControl volumeControl = (FloatControl) audioClip.getControl(FloatControl.Type.MASTER_GAIN);
                float volumeLevel = -20.0f;
                volumeControl.setValue(volumeLevel);

                audioClip.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
            guiQuienSoy.setVisible(true);
            break;
            case "Register":
                        try {
                String audioFilePath = "./src/main/resources/img/sonidoBoton.wav";
                AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(audioFilePath));
                AudioFormat audioFormat = audioInputStream.getFormat();
                DataLine.Info info = new DataLine.Info(Clip.class, audioFormat);
                Clip audioClip = (Clip) AudioSystem.getLine(info);
                audioClip.open(audioInputStream);
                FloatControl volumeControl = (FloatControl) audioClip.getControl(FloatControl.Type.MASTER_GAIN);
                float volumeLevel = -20.0f;
                volumeControl.setValue(volumeLevel);
                audioClip.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
            guiRegistro.setVisible(true);
            break;
            case "Exit":
                        try {
                String audioFilePath = "./src/main/resources/img/sonidoSalir.wav";
                AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(audioFilePath));
                AudioFormat audioFormat = audioInputStream.getFormat();
                DataLine.Info info = new DataLine.Info(Clip.class, audioFormat);
                Clip audioClip = (Clip) AudioSystem.getLine(info);
                audioClip.open(audioInputStream);
                FloatControl volumeControl = (FloatControl) audioClip.getControl(FloatControl.Type.MASTER_GAIN);
                float volumeLevel = -20.0f;
                volumeControl.setValue(volumeLevel);
                audioClip.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.exit(0);
            break;

        }
    }

}
