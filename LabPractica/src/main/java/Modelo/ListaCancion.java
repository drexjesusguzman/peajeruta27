/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author benja
 */
public class ListaCancion {

    private int longitud;
    private Nodo cabeza;

    public ListaCancion() {
        longitud = 0;
        this.cabeza = null;
    }

    public void insertarCancion(Cancion dato) {
        Nodo nodo = new Nodo(dato);
        nodo.setSiguiente(cabeza);
        cabeza = nodo;
        longitud++;
    }

    public void insertarFinal(Cancion dato) {
        Nodo nodo = new Nodo(dato);
        if (cabeza == null) {
            cabeza = nodo;
        } else {
            Nodo puntero = cabeza;
            while (cabeza.getSiguiente() != null) {
                puntero = puntero.getSiguiente();
            }
            puntero.setSiguiente(nodo);
        }
        longitud++;
    }

    public void insertarDespues(int posicion, Cancion dato) {
        Nodo nodo = new Nodo(dato);
        if (cabeza == null) {
            cabeza = nodo;
        } else {
            Nodo puntero = cabeza;
            int contador = 0;
            while (contador < posicion && puntero.getSiguiente() != null) {
                puntero = puntero.getSiguiente();
                contador++;
            }
            nodo.setSiguiente(puntero.getSiguiente());
            puntero.setSiguiente(nodo);
        }
        longitud++;
    }

    public Cancion getDato(int posicion) {
        if (cabeza == null) {
            return null;
        } else {
            Nodo puntero = cabeza;
            int contador = 0;
            while (contador < posicion && puntero.getSiguiente() != null) {
                puntero = puntero.getSiguiente();
                contador++;
            }
            if (contador != posicion) {
                return null;
            } else {
                return puntero.getDato();
            }
        }
    }

    public int getLongitud() {
        return longitud;
    }

    public boolean isVacio() {
        if (cabeza == null) {
            return true;
        } else {
            return false;
        }
    }

    public void eliminarPrincipio() {
        if (cabeza != null) {
            Nodo primero = cabeza;
            cabeza = cabeza.getSiguiente();
            primero.setSiguiente(null);
            longitud--;
        }
    }

    public String toString() {
        String datosLista = "";
        Nodo nodo = null;
        Nodo aux = null;
        if (cabeza != null) {
            nodo = cabeza;
        }
        for (int i = 0; i < longitud; i++) {
            datosLista += "   " + nodo.getDato();
            aux = nodo.getSiguiente();
            nodo = aux;
        }
        return datosLista;
    }
}
