/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import com.csvreader.CsvWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author benja
 */
public class RegistroInventarioCSV implements RegistroDatos {

    private ArrayList<Inventario> listaInventario;
    private File archivo;

    public RegistroInventarioCSV() {
        this.listaInventario = new ArrayList();
        this.archivo = new File("Inventario.csv");
        if (this.archivo.exists()) {
            this.leerCSV();
        }
    }

    public void escribirCSV() {
        if (!this.listaInventario.isEmpty()) {
            try {
                Charset cs = Charset.forName("UTF-8");
                CsvWriter escribir = new CsvWriter(new FileWriter(archivo, cs), ',');
                escribir.setDelimiter(',');
                for (Inventario inventario : listaInventario) {
                    String[] estudios = {inventario.getPlaca(), inventario.getMarca(),inventario.getModelo(),inventario.getTipo(),inventario.getEstado(),inventario.getAccesorios()};
                    try {
                        escribir.writeRecord(estudios);
                    } catch (IOException ex) {
                        System.err.println("Error al escribir el archivo");
                    }
                }
                escribir.close();
            } catch (IOException ex) {
            }
        }
    }

    public void leerCSV() {
        try {
            Scanner scanner = new Scanner(new FileReader(this.archivo));
            String line;
            this.listaInventario = new ArrayList<>();

            while (scanner.hasNext()) {
                line = scanner.nextLine();
                String[] resultado = line.split(",");
                Inventario inventario = new Inventario();
                inventario.setPlaca(resultado[0]);
                inventario.setMarca(resultado[1]);
                inventario.setModelo(resultado[2]);
                inventario.setTipo(resultado[3]);
                inventario.setEstado(resultado[4]);
                inventario.setAccesorios(resultado[5]);
                this.listaInventario.add(inventario);
            }
        } catch (FileNotFoundException ex) {
            System.err.println("Error al leer el archivo");
        }
    }

    @Override
    public String agregar(Object objeto) {
        Inventario inventario = (Inventario) objeto;
        if (this.buscar(inventario.getPlaca()) == null) {
            if (this.listaInventario.add(inventario)) {
                this.escribirCSV();
                return "Inventario agregado con exito";
            } else {
                return "Error al registrar el Inventario";
            }
        }
        return "El Inventario ya existe";
    }

    @Override
    public Object buscar(String placa) {
        System.out.println(placa);
        System.out.println("Entrando al metodo");
        for (int i = 0; i < listaInventario.size(); i++) {
            System.out.println(listaInventario.get(i));
            if (listaInventario.get(i).getPlaca().equalsIgnoreCase(placa)) {
                return (Object) this.listaInventario.get(i);
            }
        }
        System.out.println("Va a retornar null");
        return null;
    }

    @Override
    public String eliminar(Object inventario) {
        if (this.listaInventario.remove((Inventario) inventario)) {
            this.escribirCSV();
            return "Inventario eliminado con exito";
        }
        return "Error al eliminar el Inventario";
    }

    public String toString() {
        String salida = "Lista de inventario: \n";
        for (Inventario inventario : this.listaInventario) {
            salida += inventario + "\n";
        }
        return salida;
    }

    public String[][] getDatosTabla() {
        String[][] matrizTabla = new String[this.listaInventario.size()][Inventario.ETIQUETAS_INVENTARIO.length];
        for (int f = 0; f < this.listaInventario.size(); f++) {
            for (int c = 0; c < matrizTabla[0].length; c++) {
                matrizTabla[f][c] = this.listaInventario.get(f).setDatosInventario(c);
            }
        }
        return matrizTabla;
    }

}
