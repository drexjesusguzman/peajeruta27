/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author benja
 */
public class VentanillaPeaje {

    private ArrayList<Carro> listaCarro;
    private int longitud;
    private Nodo frente;
    private Nodo fin;
    private String lista;

    public VentanillaPeaje() {
        longitud = 0;
        frente = null;
        fin = null;
    }

    public void encolar(Carro carro) {
        Nodo nodo = new Nodo(carro);
        if (frente == null) {
            frente = nodo;
            fin = nodo;
            lista+=frente.getDato().toString()+"\n";

        } else {
            fin.setSiguiente(nodo);
            fin = nodo;
            lista+=fin.getDato().toString()+"\n";
        }
        longitud++;
    }

    public Nodo getCarro(int posicion) {
        int contador = 0;
        if (frente == null) {
            return null;
        } else {
            Nodo puntero = frente;
            while (contador < posicion && puntero.getSiguiente() != null) {
                puntero = puntero.getSiguiente();
                contador++;
            }
            if (contador != posicion) {
                return null;
            } else {
                return new Nodo(puntero.getDato());
            }
        }
    }

    public int getLongitud() {
        return longitud;
    }

    public boolean isVacio() {
        return frente == null;
    }

    public Carro atender() {
        Carro carro = null;
    if (frente != null) {
        carro = frente.getDato();
        frente = frente.getSiguiente();
        longitud--;
        if (longitud == 0) {
            fin = null;
        }
    }
    return carro;
    }
    
    public Carro getFrante() {
        if (frente != null) {
            return frente.getDato();
        } else {
            return null;
        }

    }

    public String getLista() {
        return lista;
    }
    
    
    public String toString() {
        String datosLista = "";
        Nodo producto = null;
        Nodo auxiliar = null;
        if (frente != null) {
            producto = frente;
        }
        for (int elemento = 0; elemento < longitud; elemento++) {
            datosLista += producto.getDato().toString() + "\n";
            auxiliar = producto.getSiguiente();
            producto = auxiliar;
        }
        return datosLista;
    }
}
