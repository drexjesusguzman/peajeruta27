/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author Drexler Guzman
 */
public class Nodo {
    
    private Carro dato;
    public Nodo siguiente;

    public Nodo(Carro dato) {
        this.dato = dato;
    }

    public Carro getDato() {
        return dato;
    }

    public void setDato(Carro dato) {
        this.dato = dato;
    }

    public Nodo getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(Nodo siguiente) {
        this.siguiente = siguiente;
    }
    
}
